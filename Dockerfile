FROM python:3.7-slim-buster as builder

# Always set a working directory
WORKDIR /app
# Sets utf-8 encoding for Python
ENV LANG=C.UTF-8
# Turns off writing .pyc files. Superfluous on an ephemeral container.
ENV PYTHONDONTWRITEBYTECODE=1
# Seems to speed things up
ENV PYTHONUNBUFFERED=1

# Ensures that the python and pip executables used
# in the image will be those from our virtualenv.
ENV PATH="/venv/bin:$PATH"


RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc \
    && apt-get clean

# Setup the virtualenv
RUN python -m venv /venv

COPY requirements.txt requirements.txt

RUN  pip install torch==1.3.1+cpu torchvision==0.4.2+cpu \
 -f https://download.pytorch.org/whl/torch_stable.html

RUN pip install --no-cache-dir \
                -r requirements.txt

# slim app container
#
FROM python:3.7-slim-buster AS app

# Extra python env
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PATH="/venv/bin:$PATH"

WORKDIR = /app

# copy in Python environment
COPY --from=builder /venv /venv


COPY app.py ./
COPY predict_functions.py ./
COPY test_app.py ./
COPY wsgi.py ./
COPY export.pkl ./


EXPOSE 5000
# ADD ../scripts /
ENTRYPOINT [ "gunicorn","--bind","0.0.0.0:5000","wsgi:app" ]
