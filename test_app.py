# test_hello.py
from app import app
from flask import json


# If a blank GET request is sent to the the base URL
# the response should be a string that says 'API UP!'
def test_hello():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert response.data == b'API UP!'


# if a user sends a good request then the response code
# the response code should be 200 and a json should be sent back
def test_api_good_data():
    response = app.test_client().post(
        '/api',
        data=json.dumps({"text": "Great honor, I think?  Mark Zuckerberg recently stated that “Donald J. \
                                           Trump is Number 1 on Facebook. Number 2 is Prime Minister Modi of India.”\
                                            Actually, I am going to India in two weeks. Looking forward to it! "
                         }),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert data['predicted_class'] == "Conservative"


# if a user sends a JSON with non string data as the text key value
# the response code should be 400 and a error message should be sent
def test_api_not_string_data():
    response = app.test_client().post(
        '/api',
        data=json.dumps({"text": 7
                         }),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 400
    assert data['error'] == "Bad request"


# if a user sends a JSON without a text key
# the response code should be 400 and an error message should be sent.
def test_api_bad_json_key_data():
    response = app.test_client().post(
        '/api',
        data=json.dumps({"not_text": "Great honor, I think?  Mark Zuckerberg recently stated that “Donald J. \
                                           Trump is Number 1 on Facebook. Number 2 is Prime Minister Modi of India.”\
                                            Actually, I am going to India in two weeks. Looking forward to it! "
                         }),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 400
    assert data['error'] == "Bad request"
