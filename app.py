from flask import Flask, request, jsonify, abort, make_response

from predict_functions import *

app = Flask(__name__)


# Taken from "https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask"
@app.errorhandler(400)
def bad_request(abort):
    return make_response(jsonify({'error': 'Bad request'}), 400)


# home url, returns API UP
@app.route('/', methods=['GET'])
def confirm_server_function():
    return "API UP!"


# api prediction url, returns a prediction from a POST request
@app.route('/api', methods=['POST'])
def answer_api_request():
    # extract json from the request save as variable
    request_json = request.json

    # if the correct key is in the json move on to next step
    # else return error json
    if "text" in request_json:

        # get text from the json
        request_text: str = extract_text_from_request(request_json)

        # if content of text key is a string advance to next step
        if isinstance(request_text, str):

            # take text and predict class
            prediction = predict_text_political_class(request_text=request_text)

            # convert model prediction to a dictionary
            response = prediction_to_dict(prediction=prediction)

            # return the response as a JSON
            return jsonify(response)

        # return error
        else:
           abort(400)

    # return error
    else:
        abort(400)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
